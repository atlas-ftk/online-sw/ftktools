/********************************************************/
/*                                                      */
/********************************************************/

#ifndef READOUT_MODULE_PU_H
#define READOUT_MODULE_PU_H 

#include <string>
#include <vector>
#include <mutex>
#include <atomic>
#include <map>
#include <thread>
#include <condition_variable>

//Common
#include "RunControl/Common/RunControlBasicCommand.h"
#include <RunControl/Common/RunControlCommands.h>
#include <RunControl/Common/Controllable.h>
#include "RunControl/Common/CommandSender.h"
#include "dal/util.h"
#include "dal/Partition.h"
#include "ipc/partition.h"
#include "ipc/core.h"
#include "ROSCore/ReadoutModule.h"
#include "is/info.h"
#include "oh/OHRootProvider.h"
#include "oh/OHRawProvider.h"
#include "RunControl/Common/OnlineServices.h"
#include "config/Configuration.h"
#include "DFdal/RCD.h"
#include "DFdal/ReadoutConfiguration.h"
#include "is/infoT.h"
#include "ftktools/dal/ReadoutModule_Manager.h"
#include "ftktools/dal/managerNamed.h"
#include <DFThreads/DFThread.h>
#include <DFThreads/DFFastMutex.h>
#include "ers/ers.h"
#include "RunControl/Common/UserExceptions.h"

namespace daq {
namespace ftk {

  class ReadoutModule_Manager : public ROS::ReadoutModule
  {

  public: //Constructor and destructor

    ///Constructor (NB: executed at CONFIGURE transition)
    ReadoutModule_Manager();

    ///Destructor (NB: executed at UNCONFIGURE transition)
    virtual ~ReadoutModule_Manager() noexcept;

	public: //Overloaded methods inherited from ROS::ReadoutModule or ROS::IOMPlugin

    /// Set internal variables (NB: executed at CONFIGURE transition)
    virtual void setup(DFCountedPointer<ROS::Config> configuration) override;

    /// Get the list of channels connected to this ReadoutModule
    virtual const std::vector<ROS::DataChannel *> *channels() override;

    /// Reset internal statistics
    virtual void clearInfo() override;

    /// Set the Run Parameters
    //virtual void setRunConfiguration(DFCountedPointer<Config> runConfiguration);

    /// Get values of statistical counters
    // virtual DFCountedPointer<Config> getInfo();

    /// Get access to statistics stored in ISInfo class
    // virtual ISInfo* getISInfo();


  public: //Overloaded methods inherited from RunControl::Controllable

    /// RC configure transition
    virtual void configure(const daq::rc::TransitionCmd& cmd) override;

    /// RC connect transition: 
    virtual void connect(const daq::rc::TransitionCmd& cmd) override;

    /// RC start of run transition
    virtual void prepareForRun(const daq::rc::TransitionCmd& cmd) override;

    /// RC stop transition
    //virtual void stopDC(const daq::rc::TransitionCmd& cmd) override;
    // virtual void stopROIB(const daq::rc::TransitionCmd& cmd) override;
    // virtual void stopHLT(const daq::rc::TransitionCmd& cmd) override;
    virtual void stopRecording(const daq::rc::TransitionCmd& cmd) override; 
    // virtual void stopGathering(const daq::rc::TransitionCmd& cmd) override; 
    // virtual void stopArchiving(const daq::rc::TransitionCmd& cmd) override; 

    /// RC unconfigure transition 
    virtual void unconfigure(const daq::rc::TransitionCmd& cmd) override;

    /// RC disconnect transition: 
    virtual void disconnect(const daq::rc::TransitionCmd& cmd) override;

    /// RC subtransition (IF NEEDED)
    // virtual void subTransition(const daq::rc::SubTransitionCmd& cmd) override;

    /// Method called periodically by RC
    virtual void publish(void) override;

    /// Method called periodically with a longer interval by RC
    virtual void publishFullStats();

    /// RC HW re-synchronization request
    virtual void resynch(const daq::rc::ResynchCmd& cmd);
	
		/// Function used to receive the usercommands
		void user(const daq::rc::UserCmd& cmd);

		///Function used to send the usercommands
		void user_sender(const std::string cmd_name, const std::vector<std::string>& cmd_params);

		///Method used to send the Recovery State trasnition to the FTK segment
		void sendTrasnition(int curr_state);

		///Method that will continuously check from a new thread which is the internal state of all 
		///the FTK RMs and call the FTK internal transitions properly   
		void RecoveryMonitor();

		//Method used for the initialization of the map containing RMs UID and corresponding FTK internal state
		void InitializeMap();

		//checks if in running state
		bool isRunning();

	public: //RM specific public functions

    ///Method used to make available the RM name to ERS log
    inline std::string name_ftk() { return m_name; }

  /// Thread safe RMManager map class.
  /// It contains the RMs pointers, the ftk internal FSM states and the methods to work safely on them
  class TSafeMap
  {
   public:
     /// Costructor
     TSafeMap();

     /// Memory safe destructor
     ~TSafeMap();

     /// AddRM method of TSafeMap class.
     /// Thread safe method to fill the map 
     //void AddRM(const daq::df::ReadoutModule*, int);
	 	 void AddRM(std::string, int);

		 //Method used to update the state of a given RM. (passing the RM UID and the int corresponding to the new state)
		 void updateState(std::string, int);

		 ///checks that the values all all the same and return the state value
		 int which_state();

	 	 ///method to print all the element of the map 
	 	 void PrintContent();

     /// Empty method of TSafeMap class.
     void mapClear();

		 inline std::string name_ftk() { return "Manager::TSafeMap"; }

   private:
     std::map< std::string, int > 				m_data;
     DFFastMutex*                      		m_accessMutex;

		 ///checks that all the values of all the keys are the same inside the map
		 bool all_equal();
	
  };

  private: //Common variables

    DFCountedPointer<ROS::Config>       	m_configuration;    					/**< Configuration Object, Map Wrapper */
    std::string                         	m_isServerName;     					/**< IS Server				*/
    IPCPartition                        	m_ipcpartition;     					/**< Partition				*/
    uint32_t                            	m_runNumber;        					/**< Run Number				*/
    std::string                         	m_name;												/**< RM name					*/
		std::string														m_appName;										/**< Application name          */
		std::string 													m_segment;										///< Name of the Segment containing the Manager
		bool 																	m_running;										///< Flag to check that we are in the running state
		bool																	m_debugMode;									///< Variable storing the state of the DebugMode oks parameter
		bool																	m_perform_recovery;						///< Variable storing the state of the performRecovery oks parameter
    bool                                  m_recovery_failed;            ///< Flag to check if a recovery transition has not completed correctly
    bool                                  m_recovery_ongoing;           ///< Flag to check if the recovery procedure is ongoing or if it is stopped (e.g. by errors)
    std::unique_ptr<managerNamed>       	m_managerNamed;     					///< Access IS via schema
		std::vector<ROS::DataChannel *>  			m_dataChannels;
		TSafeMap															m_state;											///< Map THsafe for the storage of the RMs states
		bool																	m_monitorCheck;								///< Flag for the spawning of the Monitoring thread
		std::mutex 														mtx;													///< Mutex for the Monitoring thread
		std::condition_variable 							m_cvar;												///< Condition variable or the Monitoring thread
		std::vector<std::string>							m_recovery_params;						///< Vector containing the Application and ROL names (to be recovered)
		std::map<int, std::string> 						m_transitions {
		{0,"stopDC"},
		{1,"stopROIB"},
    {2,"stopHLT"},
    {3,"stopRecording"},
    {4,"stopGathering"},
    {5,"stopArchiving"},
		{6,"disconnect"},
		{7,"unconfigure"},
		{8,"configure"},
		{9,"connect"},
    {10,"checkConnect"},
		{11,"startNoDF"},
		{12,"prepareForRun"}
		};
	
  };

	 inline const std::vector<ROS::DataChannel *> *ReadoutModule_Manager::channels()
  {
 	  return &m_dataChannels;
 	}

   inline void ReadoutModule_Manager::clearInfo() { ERS_LOG("ReadoutModule_Ambslp: Clear histograms and counters"); }

} // namespace ftk
} // namespace daq
#endif // READOUT_MODULE_PU_H
 
