02-09-2019 Nicolo' Biesuz <nbiesuz@cern.ch>

	Updated default configuration for AMB: correct FW versions, updated dummy hit value, fixed freeze severity

6-06-2019 Tova Holmes <tholmes@cern.ch>
	
	Added 2DF support and brought options up to current standards

10-06-2017 Tyler Burch <tyler.james.burch@cern.ch>

	DF22 option added - df in lab4. Allows for easier creation of p1 partitions at lab4 if needed.
	* options_df.py - df22 option added
	* process_df.py - proper handling of option

	Better handling of df dictionary import
	* df_tools.py - Checks if /det/ftk/tools exists (p1). If so imports that, if not imports ftkmaker version
	
10-06-2017 Tyler Burch <tyler.james.burch@cern.ch>

	Changes to allow for running outside of local directory
	* ftk_maker.py - import ftkmaker package version of df_tools (import df_tools -> import ftkmaker.df_tools)
	* process_df.py - fixed imports for df_tools, df_configurations
	* process_vme.py - fixed imports for aux, amb, ssb, slot configurations
	* df_tools.py - load df dictionary from /det/ftk/tools if it exists, otherwise from ftkmaker package

8-11-2017 Tyler Burch <tyler.james.burch@cern.ch>

	* options_df.py: Changed defaults to all be p1 versions
	* process_df.py: Stopped generation of pseudodata links if not using pseudodata
	* df_tools.py: Changed DF dictonary to be the one in /det/ftk/tools/DF_cabling when running at p1. In l4 goes ${CWD}/dfDict/dfDict.json

8-10-2017 Tyler Burch <tyler.james.burch@cern.ch>

	* options_df.py: Removed SCTonly option - if needed automatically find by wildcards new IM_Pseudodata/IM_Luts generation files
	* process_df.py: Removed SCTonly references

7-31-2017 Tyler Burch <tyler.james.burch@cern.ch>

	* options_df.py: added new OKS parameters 'IPBusEmulatorMode', 'failOnBadLink', 'DF_b0fTimeoutThreshold', and 'usePseudoData'
		added new parameters for FTKmaker DF integration: 'GenerateConfiguration', 'PseudoData_directory',LUTs directory, 'txConfigOption'
		removed 'IM_SLINKChannelMask' (deprecated) and 'Pseudo_Data' (replaced by OKS usePseudoData)
		clarified df-shelfs mapping to explain the physical mapping

	* process_df.py: fixed df-shelfs mapping to match physical mapping (alternating left and right from center)
		added calls to df_tools.py functions to generate configuration files
		removed IM_SLINKChannelMask references
        lab4 functionality added - forces DF to be called DF22

	* df_tools.py: inital commit - functions to be imported elsewhere (generally to create config files)

	* ftk_maker.py: calls several df_tools.py functions to generate config files where necessary
	   	Now will stop if no Pseudodata_directory or LUTs_directory
        creates symlink to modulelist file (to remain centralized, edited only by experts)

	* df_configurations.py: Corrected a few default values

	* notes/outstanding:
        	should force usePseudodata if lab4
        	Should add 'make for atlas' option to generate 1 RCD and force usePseudodata to be false
