########################################################################
### Dictionaries for different AMB configurations.
########################################################################
### Assumes a default of running the full slice at P1
### In current set up, each option should be identical to the name of
### the attribute to set in the xml file. This is applied *only* to AUX
### so no conflicts with other boards should arise.
########################################################################

# Default options (only need to be set if different from those defined in the schema)
default_amb_dic = {
    'PhysAddress':              0,
    'HITFWVersion':             0x3ca5296c,
    'ROADFWVersion':            0x24a50b93,
    'VMEFWVersion':             0x000005c1,
    'CTRLFWVersion':            0x0000000a,
    'LAMBFWVersion':            0x7,
    'DryRun':                   0,
    'NumberOfPatternsToCheck':  0,
    'ForceWrite':               0,
    'DummyHit':                 0x2a302530,
    'UseBlock':                 1,
    'FreezeSeverity':           0x4,
    'DCDCSetVolt':              0x2e3,
    'AddTestPattern':           0,
    'OverrideSpecialPattern':	1,
    'DumpFolder':	        '"/tmp"',
    'LAMBMask':	                0xf,
    'InitialThreshold':	        15,
    'Threshold':                7,
    'CheckBank':                0,
    'BypassBus':	        0x24,
    'PCBversion':	        1,
    'DVmajority':               1,
    }

# SliceA dictionary
##################################################################
sliceA_amb_dic = {
    'PhysAddress':              0,
    'HITFWVersion':             0x3ca5296c,
    'ROADFWVersion':            0x24a50b93,
    'VMEFWVersion':             0x000005c1,
    'CTRLFWVersion':            0x0000000a,
    'LAMBFWVersion':            0x7,
    'DryRun':                   0,
    'NumberOfPatternsToCheck':  0,
    'ForceWrite':               0,
    'DummyHit':                 0x2a302530,
    'UseBlock':                 1,
    'FreezeSeverity':           0x4,
    'DCDCSetVolt':              0x2e3,
    'AddTestPattern':           0,
    'OverrideSpecialPattern':	1,
    'DumpFolder':	        '"/tmp"',
    'LAMBMask':	                0xf,
    'InitialThreshold':	        15,
    'Threshold':                7,
    'CheckBank':                0,
    'BypassBus':	        0x24,
    'PCBversion':	        1,
    'DVmajority':               1,
    }

# Slice2 dictionary
##################################################################
slice2_amb_dic = {
    'PhysAddress':              0,
    'HITFWVersion':             0x3ca5296c,
    'ROADFWVersion':            0x24a50b93,
    'VMEFWVersion':             0x000005c1,
    'CTRLFWVersion':            0x0000000a,
    'LAMBFWVersion':            0x7,
    'DryRun':                   0,
    'NumberOfPatternsToCheck':  0,
    'ForceWrite':               0,
    'DummyHit':                 0x2a302530,
    'UseBlock':                 1,
    'FreezeSeverity':           0x4,
    'DCDCSetVolt':              0x2e3,
    'AddTestPattern':           0,
    'OverrideSpecialPattern':	1,
    'DumpFolder':	        '"/tmp"',
    'LAMBMask':	                0xf,
    'InitialThreshold':	        15,
    'Threshold':                7,
    'CheckBank':                0,
    'BypassBus':	        0x24,
    'PCBversion':	        1,
    'DVmajority':               1,
    }

#######################################################################
### Configurations to be turned on by various options
#######################################################################

# Dictionary for options specific to Lab 4
lab4_dic = {
    }


#######################################################################
### Other default values to be set
#######################################################################

amb_m14_slot_overwrites = {
        1: # Crate index
            {
            19: { },
            20: { },
            }
}
