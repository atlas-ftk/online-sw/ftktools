""" This module hosts the fuction supposed to generate ReadoutModules (RMs) for flic
"""
# TODO

def optional_includes(self):
    return []


def execute(rcd, ftkProj, index):
  """ Creates the ReadoutModules (RMs) for FLIC. 
  
  This function is called by the ftk_maker.py application that manages the 
  creation of FTK partition and segments. This function is supposed to create 
  the FLIC RMs and to append them to the rcd object received as parameter.
  The required OKS objects are accessible via the ftkProj that is an instance 
  of the FTKProject class.
  Nota bene: 
   - the user shall explicitly append the RMs to the rcd instance
   - the user could tweak the rcd object 

  Input Parameters:
    rcd       An RCD instance. It will hosts all the RMs created by this function
    ftkProj   An instance of the FTKProject class instantiated by the caller
              The option dictionary is available via: ftkProj.args
    index     The index in the Dataformatter shelves lists. The function is 
              supposed to access configuration data at row = index of the 
              ftkProj.args["flic-shelves"] dictionary entry, i.e.
              ftkProj.args["flic-shelves"][index]

  Useful objects from ftkProj:
    args      The FTK option dictionary. E.g.: ftkProj.args["flic-shelves"] provides 
              a list of tuples with the format: ["shelfName", "computer", "shelfMap"]
    dal_flic  The dal module used to instantiate objects defined in the flic schema       
  """

  s = "\033[1;37m" + "  [process_flic.execute]" + "\033[0m"
  print s, "Making RCD", rcd.id, "running on", rcd.RunsOn.id

  # get the options
  opts  = ftkProj.args
  shelf = opts["flic-shelves"][index]
  print s, "Processing crate:", shelf

  # Loop over the map
  shelfName=shelf[0]
  shelfMap =shelf[2]

  # Sanity check:
  # Make sure number of F's in shelfMap matches number of tuples
  # with IP information
  assert shelfMap.count('F') == len(opts["flic-ips"])
  iflic = 0
  
  for slot in range(0, len(shelfMap)):
    suffix = "%s" % (shelfName)

    # Occupied slots
    if shelfMap[slot] == "F":
      rm_id = "FLIC-" + suffix 
      print "%s Making %s@ReadoutModule_flic" % (s, rm_id)
      RM_flic = ftkProj.dal_flic.ReadoutModule_flic(rm_id)

## ADD SSB SPECIFIC CODE HERE >>>>>>>>>>>>>>>>>>>>>>>>>
 
      # For example
      RM_flic.Slot = slot

      if ftkProj.args["UseMultithreading"]:
        RM_flic.ParallelConfigure = False
        RM_flic.ParallelConnect = False
        RM_flic.ParallelPrepareForRun = False
        RM_flic.ParallelStopROIB = False
        RM_flic.ParallelStopDC = False
        RM_flic.ParallelStopHLT = False
        RM_flic.ParallelStopRecording = False
        RM_flic.ParallelStopGathering = False
        RM_flic.ParallelStopArchiving = False
        RM_flic.ParallelDisconnect = False
        RM_flic.ParallelUnconfigure = False
        RM_flic.ParallelMonitoring = False
        RM_flic.SeparatePublishThreads = False
        RM_flic.ProtectStart = True
        RM_flic.ProtectStop = True
        RM_flic.SerializeRCDMonitoring = True

## <<<<<<<<<<<<<<<<<<<<<<<<<<<<< ADD FLIC SPECIFIC CODE HERE

      RM_flic.FlicIP = opts["flic-ips"][iflic][0]
      RM_flic.Port   = opts["flic-ips"][iflic][1]
      RM_flic.HostIP = opts["flic-ips"][iflic][2]
      iflic += 1
      RM_flic.Verbose= opts["Verbose"]
      RM_flic.LoopbackMode= opts["LoopbackMode"]
      RM_flic.UseAsSSBe= opts["UseAsSSBe"]
      RM_flic.TracksPerRecord= opts["TracksPerRecord"]
      RM_flic.DelayBetweenRecords= opts["DelayBetweenRecords"]
      RM_flic.TotalRecords= opts["TotalRecords"]
      RM_flic.DelayBetweenSyncAttempts= opts["DelayBetweenSyncAttempts"]
      RM_flic.TotalSyncAttempts= opts["TotalSyncAttempts"]
      RM_flic.LinksInUse= opts["LinksInUse"]
      RM_flic.TotalROSLinkResets= opts["TotalROSLinkResets"]
      RM_flic.IgnoreROSLinks= opts["IgnoreROSLinks"]
      RM_flic.ReloadFirmware= opts["ReloadFirmware"]
      RM_flic.ReloadSRAMs= opts["ReloadSRAMs"]
      RM_flic.CaptureCondition= opts["CaptureCondition"]
      RM_flic.MonitoringChannel= opts["MonitoringChannel"]

      # Add the ReadoutModule to the RCD
      rcd.Contains.append(RM_flic)

    # Empty slots
    else:
      print s, "Skip slot", slot
