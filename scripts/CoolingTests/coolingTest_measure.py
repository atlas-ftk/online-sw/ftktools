#!/usr/bin/env tdaq_python

import AMBDCDCUtility
import subprocess
import time
import commands
import sys
import os
from subprocess import CalledProcessError

#Definition of the colors to be used for the output
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

#constants
LAMBTEMPREAR = 0x00000030
LAMBTEMPFRONT = 0x00000034

#Parameter definition
Slots = [ 2 ]
#Slots = [2, 3, 4, 5, 7, 8, 9, 10, 12, 13, 14, 15, 17, 18, 19, 20]
SSBSlots = []
#SSBSlots = [6, 11, 16, 21]
cycles=8;

# this list represets how many times data are collected and  the time after the
# previous step to wait before the collection
Delays = [0, 60, 60*2, 60*5, 60*5, 60*5, 60*5, 60*5, 60*5, 60*5 ]
#Delays = [0, 60]

# file descrciptor where the logs are saved
powerLogFile = None

def retrieveData() :
  for slot in Slots :
    print bcolors.OKBLUE + "Slot: %d" %slot + bcolors.ENDC
    if slot<10:
      RMN = RMName + "0" + str(slot)
    else:
      RMN = RMName + str(slot)
    
    #starting the reding of the AMB boards info
    print bcolors.FAIL + "AMB info:" + bcolors.ENDC
    os.system("ambslp_status_main --slot %d | head -n 24 | tail -n 5" %slot) #print temperature from the board
    os.system("ambslp_status_main --slot %d | tail -n 1" %slot)              #print the board rate
    res = AMBDCDCUtility.ReadDCDCInfo(slot)
    # read the temperatures of the LAMBs from the rear and fron sensors
    val = AMBDCDCUtility.VMEPeek(slot, LAMBTEMPREAR)
    TempRear = [(val>>(8*i))&0xff for i in xrange(4)]
    val = AMBDCDCUtility.VMEPeek(slot, LAMBTEMPFRONT)
    TempFront = [(val>>(8*i))&0xff for i in xrange(4)]
    line = time.strftime("%Y-%m-%d_%H:%M")
    line += ", 0x%08x" % AMBDCDCUtility.VMEPeek(slot, 0x2168)
    line += ", "+str(slot)
    for i in xrange(4) :
      line += ", %d, %d" % (TempFront[i], TempRear[i])
    for v in res :
      line += ", "+str(v)

    #starting the reding of the AUX boards info
    print bcolors.FAIL + "AUX info:" + bcolors.ENDC
    os.system("./getAuxRate.sh %d" %slot)
    for f in xrange(1,7):
      p =  commands.getstatusoutput('./getAuxTemp.sh %s %s' %(slot,f))
      line += ", %s" %p[1]
      print "FPGA %s temp: \t%s" %(f,p[1])
    powerLogFile.write(line+"\n")
    powerLogFile.flush()

  #starting the reding of the SSB boards info
  for ssbslot in SSBSlots :
    print bcolors.OKBLUE + "Reading SSB Slot: %d" %ssbslot + bcolors.ENDC
    line = time.strftime("%Y-%m-%d_%H:%M")
    line += ", SSB"
    line += ", "+str(ssbslot)
    ssb =  commands.getstatusoutput('''ssb_status_main --slot %s --NEXTF 4 | grep "FPGA Temp" | awk '{print $NF}' | tr "C" "," | tr "\n" " " | head --bytes -2''' %ssbslot )
    line += ", %s" %ssb[1]
    powerLogFile.write(line+"\n")
    powerLogFile.flush()
  return None

def updateDummyHit(dhval):
  for slot in Slots :
    AMBDCDCUtility.VMEPoke(slot, 0x2168, dhval)
  time.sleep(1)
  return None

#################################
#             main              #
#################################
if len(sys.argv[1:]) == 0 :
  powerLogFile = open("power_report.csv", "w")
else :
  powerLogFile = open(sys.argv[1], "w")
# print the header
powerLogFile.write("Time, DummyHit, Board, TempLAMB0a, TempLAMB0b, TempLAMB1a, TempLAMB1b, TempLAMB2a, TempLAMB2b, TempLAMB3a, TempLAMB3b, Vin, Vout, Iout, Temp, FPGA1Vout, FPGA1Iout, FPGA2Vout, FPGA2Iout, V0in, V0out, I0out, Power0, T0a, T0b, V1in, V1out, I1out, Power1, T1a, T1b, V2in, V2out, I2out, Power2, T2a, T2b, V3in, V3out, I3out, Power3, T3a, T3b, AUXFPGA1, AUXFPGA2, AUXFPGA3, AUXFPGA4, AUXFPGA5, AUXFPGA6\n")

retrieveData()
print bcolors.FAIL + "Starting.." + bcolors.ENDC
for dt in range(cycles) : # loop over the delays
  print "Waiting 60s before next read. We are at the cycle number  %d" % dt
  time.sleep(60) # wait
  # collect data for all the slots and store it in the table
  retrieveData() 
  
powerLogFile.close()
