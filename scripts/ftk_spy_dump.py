#!/usr/bin/env tdaq_python


"""SpyBuffer dumper

This is the FTK spybuffer dumper from emon.
"""

__author__ = "Simone Sottocornola and Misha Lisovyi"
__version__ = "$Revision: 1.0 $"
__date__ = "$Date: 2018/03/XX $"

import ers
import ipc
import emon
import os
import sys
import getopt
import eformat
import eformat.dump
import libpyeformat
import libpyeformat_helper
import time
import datetime as dt

class bcolors:
    HEADER = '\033[95m'
    OKWHITE = '\033[97m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def Usage():
  print bcolors.OKBLUE + "Get events from emon and dump the data" + bcolors.ENDC
  print "    -h               Help"
  print bcolors.BOLD + "                   Selection criteria" + bcolors.ENDC
  print "    -p part          Partition name"
  print "    -F freeze_mask   The mask to be set on freeze status in the common SpyStatus word (def=ignore mask)"
  print "    -t boardTypes    Comma-separated list of board types to be dumped (def=FTK,i.e. all)"
  print bcolors.BOLD + "                   Configuration" + bcolors.ENDC
  print "    -D ms            Event timeout (def=1000ms)"
  print "    -e evts          Number of events to sample (def=1)"
  print bcolors.BOLD + "                   Output options" + bcolors.ENDC
  print "    -o dirname       Dump individual spybuffers into files in a SpyDump_YYYYMMDD_HHMMSS subdirectory in the provided directory"
  print "    -S               Only print the sampler names and stop"
  print "    -r               Dump robs ids"
  print "    -R               Dump raw data words of the eformat.FullEventFragment"
  print "    -P level         Eformat pretty print: 1=dump full event header, 2=dump robs header [you will want 2 in most cases]"
  print "    -f fname         [debug] Dump the whole eformat.FullEventFragment into output file"
  print "    -w delay-time    [debug] Delay in ms, the monitor should wait for each"
  print "                               event to simulate processing (default 0)"
  sys.exit(2)

###############################################################################################

BoardTypes = {'FTK': 0, 'IM': 1, 'DF': 2, 'AUX': 3, 'AMB': 4, 'SSB': 5, 'FLIC': 6, 'OTHER': 0xf}
Position   = {'IN': 0, 'OUT': 1, 'OTHER_POSITION': 3}
DataType   = {'DATA': 0, 'SPYBUFFER': 1}

def encode_SourceIDSpyBuffer(boardType, boardNumber=0, boardInternal=0, position=3, res = 0x0):
  moduleID  = DataType['SPYBUFFER']	# 1bit Spy Buffer/ Actual Data
  moduleID <<= 3
  moduleID |= (0x7) & res		# reserved 3 bits
  moduleID <<= 4
  moduleID |= (0Xf) & boardType		# 4bit board type
  moduleID <<= 8;
  moduleID |= (0XFF) & boardNumber	# 7bit board number

  reservedID  = (0X3f) & boardInternal	# 6bit board internal ID
  reservedID <<= 2;
  reservedID |= (0X3) & position	# 2bit for position

  sourceID = libpyeformat_helper.SourceIdentifier(libpyeformat_helper.SubDetector.TDAQ_FTK, moduleID, reservedID).code()
  return sourceID


def decode_SourceIDSpyBuffer(word):
  boardType = (word >> 8) & ((1 << (11-8+1))-1)
  for boardTypeKey, boardTypeValue in BoardTypes.iteritems():
    if boardTypeValue == boardType:
      return boardTypeKey
  

###############################################################################################

pname    = 'ATLAS'
type     = 'ReadoutApplication'
names    = []
l1bits   = []
timeout  = 1000
dRobs    = False
dRaw     = False
dPretty  = 0
maxEvts  = 1
onlySamp = False
nSamplers= 1000
afile	   = False
adir     = ''
dTime    = 0
boardTypeStr='FTK'

freezeMask = -1

#Read command line parameters
try:
  opts, args = getopt.getopt(sys.argv[1:], "hrRSP:p:D:e:f:w:F:o:t:")
except getopt.GetoptError, err:
  print str(err)
  Usage()
  sys.exit(2)
try:
  if not len(opts): Usage()
  for o,a in opts:
    if    o == "-h": Usage()
    elif  o == "-D": timeout  = int(a)
    elif  o == "-r": dRobs    = True
    elif  o == "-R": dRaw     = True
    elif  o == "-S": onlySamp = True
    elif  o == "-P": dPretty  = int(a)
    elif  o == "-e": maxEvts  = int(a)
    elif  o == "-p": pname    = a
    elif  o == "-f": afile    = a
    elif  o == "-w": dTime    = int(a)
    elif  o == "-F": freezeMask    = int(a)
    elif  o == "-o": adir     = a
    elif  o == "-t": boardTypeStr = a


except(AttributeError): pass

if afile:
  # Create the output file
  fout = eformat.ostream(".", afile)

if adir:
  dirname = "{0}/SpyDump_{1}".format(adir, dt.datetime.now().strftime('%Y%m%d_%H%M%S'))
  print "Will dump spy buffers into {}".format(dirname) 
  try:
    os.mkdir(dirname)
  except:
    sys.exit("ERROR: did not manage to create {}. Exiting!".format(dirname))

# extract the list of requested board types
boardTypeList = boardTypeStr.split(',')

###############################################################################################

if __name__ == "__main__":
  # Create the partition
  partition = ipc.IPCPartition(pname)

  # Find the available samplers
  try:
    samplers = emon.getEventSamplers(partition)
    # Either dump the samplers
    if onlySamp:
      print "The samplers are:"
      for s in samplers:
        print s
      sys.exit(0)
    # Or find the samplers of the right type  
    for s in samplers:
      beg=type+":"
      if s.startswith(type+":") and ('ROS' not in s) and ('Flic' not in s) and ('Manager' not in s):
        names.append(s[len(beg):])
    names = names[:nSamplers]    
    print "Using samplers:", names    
  except ers.Issue, e:
    ers.log( e )

  if not names:
    print "No samplers were found (check that there are RCDs sampling to emon). Exiting..."
    sys.exit(1)

  # The app to sample from
  for name in names:
    print name
    address  = emon.SamplingAddress( type, name)
    print address
    
    # Selection based on freeze status
    freezeVal, freezeIgnore = (0, True) if freezeMask < 0 else (freezeMask, False)
    freezeSelection = emon.L1TriggerType(freezeVal, freezeIgnore)
    
    for boardType in boardTypeList:
      if boardType not in BoardTypes:
        print 'Unknown board type requested: {}. Skip it over...'.format(boardType)
        continue
      print boardType
    
      # Selection based on board sourceID
      sourceID = encode_SourceIDSpyBuffer(BoardTypes[boardType], 
    					boardNumber=0xFF, 
    					boardInternal=0, 
    					position=Position['OTHER_POSITION'], 
    					res = 0x0)
      print 'SourceID = 0x{:08x}'.format(sourceID)
      boardSelection = emon.StatusWord( sourceID, False)
    
      # Full selection criteria
      criteria = emon.SelectionCriteria(
            freezeSelection,
            emon.L1TriggerBits([], emon.Logic.IGNORE, emon.Origin.AFTER_VETO),
            emon.StreamTags('', [], emon.Logic.IGNORE),
            boardSelection
            )
      
      evt = 0
      fe = None
      with emon.EventIterator(partition, address, criteria) as iterator:
        while evt < maxEvts:
          evt += 1
          try:
            event = iterator.nextEvent(timeout)
          except RuntimeError:
            print "No new events available..."
            continue
          fe = libpyeformat.FullEventFragment(event)  
          stags = fe.stream_tag()
          if dTime != 0:
            time.sleep(dTime/1000.)
    
    	# Adding event to the out file
    	if afile:
    	  new_ev = eformat.write.FullEventFragment(fe)
    	  fout.write(new_ev.__raw__())
    
        if not fe:
          print "Continue with the next requested board type, if any..."
          continue
    
        # Pretty print
        if dPretty > 0:
          eformat.dump.fullevent_handler(fe)
          if dPretty >1:
            for rob in fe:
              print "-------------------------------------------------------------------------"
              eformat.dump.rob_handler(rob)
    
        # Hexdump printout
        if dRaw:
          cnt = 0
          for w in event:
            print "[%6d] 0x%08x" % (cnt, w)
            cnt = cnt + 1
    
        # Print generic informations
        print "L1id/Gid=%8d / %8d, zlib=%d sz=%6d/%6dB, #stags=%2d, #ROBs=%3d, #status=%2d" % (
          fe.lvl1_id(), fe.global_id(), fe.compression_type(), fe.fragment_size_word()*4, fe.readable_payload_size_word()*4, len(stags), fe.nchildren(), len(fe.status()))
    
        # Dump ROB IDs
        if dRobs:
          str="\tROBs: "
          for r in fe.children():
            str+="0x%08x " % r.source_id()
          print str
    
        # Dump SpyBuffers into separate files
        if adir:
          for r in fe.children():
            sourceID = r.source_id().code()
            print 'Decoded board name from the 0x{:08x} sourceID is {}'.format(sourceID,
      							decode_SourceIDSpyBuffer(sourceID))
            spyfilename = "{}/{}_0x{:08x}.txt".format(dirname, decode_SourceIDSpyBuffer(sourceID), sourceID)
            spyfout = open(spyfilename, 'w')
            for i in r.rod_data():
              spyfout.write("0x%08x\n" % i)
            spyfout.close()
    
    if afile:
      fout.close()

