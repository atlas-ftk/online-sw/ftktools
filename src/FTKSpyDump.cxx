#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ftkcommon/SourceIDSpyBuffer.h>
#include <iostream>
#include <iomanip>
#include <ftktools/SpyBufferGetter.h>


int main(int argc,char *argv[]) {

	try {
		IPCCore::init( argc, argv );
	}
	catch( daq::ipc::Exception & ex ) {
		ers::fatal( ex );
		return 1;
	}
	
	//This is the declaration part.
	CmdArgBool  help('h', "help", "Get events from emon and dump the data");
    CmdArgStr   partition_name ('p', "part", "FTKDummyLouis", "Partition name");
    CmdArgStr   dump_directory ( 'd' , "directory", "PathToDirectory", "Path to the dump directory");
    CmdLine cmd(*argv, &partition_name, &dump_directory ,NULL);
	CmdArgvIter  arg_iter(--argc, ++argv);
	cmd.set(CmdLine::NO_ABORT|CmdLine::QUIET);
	cmd.parse(arg_iter);
    
    SpyBufferGetter* getter=new SpyBufferGetter();
	
	// Create map from the partition (partition name given in the Command Line)
    if (partition_name){
        getter->setupMapFromPartition((const char*) partition_name );}
    
    //Create map from directory: (directory from which the data should be accessed given in the Command Line)
    if (dump_directory){
    getter->setupMapFromDirectory((const char*) dump_directory);}
    
	// Save all SpyBuffers in a directory, each SpyBuffer is a textfile
	getter->saveMapToDirectory("./dir/",FTKBoard::FTK);

	//Example how to access the different board registers: First explicit which type of board you are intersted in: FTK means all of them otherwise the enum is like this: enum FTKBoard {FTK=0, IM=1, DF=2, AUX=3, AMB=4, SSB=5, FLIC=6};	
    std::vector<unsigned int> *boards=getter->getBoardSpecificSourceIDs(FTKBoard::FTK);
    std::cout<<"FTKSpyDump::There are nboards with Spybuffers:: "<<boards->size()<<std::endl;

    for (int i=0;i<boards->size();i++){// Loop over the sourceID and get the spybuffers:
        std::cout<<"FTKSpyDump::Board " << std::dec << i << std::hex << "  "<<boards->at(i); // "FTKSpyDump:: Source ID
        unsigned int sourceID=boards->at(i);
        uint32_t boardTypeint = daq::ftk::decode_SourceIDSpyBuffer(sourceID).boardType;
        std::vector <unsigned int> spyBuffer=getter->getSpyBufferFromSourceID(sourceID);

        std::cout << i <<" FTKSpyDump::Number of words: "<< std::dec << spyBuffer.size()<<" , board type: "<<getter->getStringBoardTypeFromUInt(boardTypeint)<<" Spybuffer content: ";

            for(int j=0;j<spyBuffer.size();j++){//Loop over the word in the spybuffer.
                std::cout<<" "<< "0x" << std::hex << std::setfill ('0') <<std::setw (8) << spyBuffer.at(j);
            }// End Loop over the woord in spybuffer.
        std::cout<<" "<<std::endl;
    }//End Loop over the sourceID
    
    // create a post processing map object
    std::map <std::string , std::map <unsigned int, SpyBufferGetter::detailed_information_struct > > my_post_processing_information_map = getter->createMapforPostProcessing(FTKBoard::FTK);
    
    getter -> printMapforPostProcessing(my_post_processing_information_map);
    
    // access a specific struct of the map: input the map you want to work with and two keys: the category key and the board's unique identifier
    
    SpyBufferGetter::detailed_information_struct element_from_map = getter -> getMapElement(my_post_processing_information_map, "firmware", 1286);
    
    std::cout << "Float element from the map: " << element_from_map.num_float <<std::endl;
    
    // access the map that was created and modify a chosen struct, the output is a map object with the modified element
    // this is an example use of this function, this function should be implemented by the boards in order to fill in the information
    std::map <std::string , std::map <unsigned int, SpyBufferGetter::detailed_information_struct > > my_post_processing_information_map_1 = getter -> insertMapElement(my_post_processing_information_map, "firmware", 1286, 0, 0, false, " error ", " info " );
    
    getter -> printMapforPostProcessing(my_post_processing_information_map_1);
    
    SpyBufferGetter::detailed_information_struct element_from_map_1 = getter -> getMapElement(my_post_processing_information_map_1, "firmware", 1286);
    std::cout << "Floating number element from the filled in map: " << element_from_map_1.num_float <<std::endl;
    
	return 0;
}
