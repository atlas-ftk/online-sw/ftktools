#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <ftkcommon/SourceIDSpyBuffer.h>
#include <iostream>
#include <ftktools/SpyBufferGetter.h>
#include <ftktools/SSBEmonSpyParse.h>


int main(int argc,char *argv[]) {

	try {
		IPCCore::init( argc, argv );
	}
	catch( daq::ipc::Exception & ex ) {
		ers::fatal( ex );
		return 1;
	}
	
	
	//This is the declaration part. 
	CmdArgBool  help('h', "help", "Get events from emon and dump the data");
  
	CmdArgStr   partition_name ('p', "part", "FTKDummyLouis", "Partition name");
	CmdLine  cmd(*argv, &partition_name ,NULL);
	CmdArgvIter  arg_iter(--argc, ++argv);
	cmd.set(CmdLine::NO_ABORT|CmdLine::QUIET);
	cmd.parse(arg_iter);
		
	
        SpyBufferGetter* getter=new SpyBufferGetter();
	
	// Create the map from the partition:
	getter->setupMapFromPartition((const char*)partition_name ); 
	
	// Save all the spybuffer in a directory
	//getter->saveMapToDirectory("./dir/",FTKBoard::FTK);


        daq::ftk::ssb_emon_spyparse ssb_emon(getter);

        ssb_emon.printSSBSpybuffersToFile("test_ssb/","ssb_spybuffer_dump_main_FPGA");

        ssb_emon.printSSBHistosToFile("test_ssb/","ssb_spybuffer_dump_histo");

        ssb_emon.getOutspys();
    
	return 0;
}

